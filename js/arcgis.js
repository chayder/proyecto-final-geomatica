require([
    "esri/config",
    "esri/Map",
    "esri/views/MapView",
    "esri/tasks/Locator",
    "esri/Graphic",
    "esri/layers/GraphicsLayer"
], (esriConfig, Map, MapView, Locator, Graphic, GraphicsLayer) => {
    esriConfig.apiKey = "AAPK35d06fe9f28947acb19660f209b5257dv0VIv0z_jjKsvA1TdnSGCWzdLcRvU8nll6ilA4wm2wV-_-dxEUTDcnA1VEoOwM9l";

    const map = new Map({
        //basemap: "arcgis-topographic" // Basemap layer service
        //basemap: "arcgis-dark-gray"
        basemap: "arcgis-navigation-night"
    });

    const view = new MapView({
        map: map,
        center: [-74.0677305, 4.6281256], // Longitude, latitude
        zoom: 13, // Zoom level
        container: "viewDiv" // Div element
    });

    const graphicsLayer = new GraphicsLayer();
    map.add(graphicsLayer);

    //const point = { //Create a point
    //    type: "point",
    //    longitude: -118.80657463861,
    //    latitude: 34.0005930608889
    //};
    //const simpleMarkerSymbol = {
    //    type: "simple-marker",
    //    color: [226, 119, 40],  // Orange
    //    outline: {
    //        color: [255, 255, 255], // White
    //        width: 1
    //    }
    //};

    //const pointGraphic = new Graphic({
    //    geometry: point,
    //    symbol: simpleMarkerSymbol
    //});
    //graphicsLayer.add(pointGraphic);

    const locator = new Locator({
        url: "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer"
    });

    const places = ["Seleccione una categoría...", "Parks and Outdoors", "Coffee shop", "Gas station", "Food", "Hotel", "Museum", "Library", "School", "Bank", "Market", "Pharmacy", "Jazz Club", "Casino", "Zoo"];

    const select = document.createElement("select", "");
    select.setAttribute("class", "esri-widget esri-select");
    select.setAttribute("style", "width: 175px; font-family: 'Avenir Next W00'; font-size: 0.8rem; border-radius: .375rem");

    const instructions = document.createElement("button");
    instructions.textContent = "Instrucciones";
    instructions.setAttribute("class", "popup");
    instructions.setAttribute("onClick", "alerta()");
    instructions.setAttribute("style", "width: 175px; font-family: 'Avenir Next W00'; font-size: 0.8rem; border-radius: .375rem; background-color: #ffffff;");

    const popup = document.createElement("span");
    popup.setAttribute("id", "myPopup");
    popup.setAttribute("class", "popuptext");

    const instructionsTxt = document.createElement("pre");
    instructionsTxt.textContent = `                                             Instrucciones
    1. Seleccione una categoría en la lista desplegable.
    2. Seleccione una ubicación en el mapa haciendo doble click en ella.
    3. Puede visualizar información de los puntos resaltados en el mapa haciendo click en ellos.
    4. Para seleccionar una nueva ubicación haga doble click en la nueva posición deseada.`;
    instructionsTxt.setAttribute("style", "text-align: left;");

    places.forEach((p) => {
        const option = document.createElement("option");
        option.value = p;
        option.innerHTML = p;
        select.appendChild(option);
    });

    popup.appendChild(instructionsTxt);
    instructions.appendChild(popup);
    view.ui.add(instructions, "bottom-right");
    view.ui.add(select, "top-right");

    view.on("double-click", (evt) => {
        const params = {
            location: evt.mapPoint
        };

        locator.locationToAddress(params)
            .then((response) => { // Show the address found
                view.graphics.removeAll();
                graphicsLayer.removeAll();
                const address = response.address;

                const pointGraphic = new Graphic({
                    geometry: {
                        type: "point",
                        longitude: evt.mapPoint.longitude,
                        latitude: evt.mapPoint.latitude
                    },
                    symbol: {
                        type: "simple-marker",
                        color: [226, 119, 40], // Orange
                        size: "12px",
                        outline: {
                            color: [255, 255, 255], // White
                            width: 1
                        }
                    },
                    popupTemplate: {
                        title: address,
                        content: +Math.round(evt.mapPoint.longitude * 100000) / 100000 + ", " + Math.round(evt.mapPoint.latitude * 100000) / 100000,
                        location: evt.mapPoint
                    }
                });
                view.graphics.add(pointGraphic);

                showAddress(address, evt.mapPoint);
                findPlaces(select.value, evt.mapPoint); // view.center
            }, (err) => { // Show no address found
                showAddress("No address found.", evt.mapPoint);
            });

    });

    const showAddress = (address, pt) => {
        view.popup.open({
            title: address,
            content: +Math.round(pt.longitude * 100000) / 100000 + ", " + Math.round(pt.latitude * 100000) / 100000,
            location: pt
        });
    }

    // Find places and add them to the map
    const findPlaces = (category, pt) => {
        locator.addressToLocations({
            location: pt,
            categories: [category],
            maxLocations: 25,
            outFields: ["Place_addr", "PlaceName", "Distance", "Type"]
        }).then((results) => {
            //view.popup.close();
            //view.graphics.removeAll();
            results.forEach((result) => {
                result.attributes.Distance = (result.attributes.Distance / 1000).toFixed(3);
                //console.log(result.attributes);
                // view.graphics.add(
                graphicsLayer.add(
                    new Graphic({
                        attributes: result.attributes, // Data attributes returned
                        geometry: result.location, // Point returned
                        symbol: {
                            type: "simple-marker",
                            //color: "#00fa6c",
                            color: results.indexOf(result) == 0 ? "#00fa6c" : "#0685b6",
                            size: "12px",
                            outline: {
                                color: "#ffffff",
                                width: "2px"
                            }
                        },

                        popupTemplate: {
                            title: "{PlaceName}", // Data attribute names
                            content: "{Type}: {Distance} Km. <br><br> {Place_addr}"
                        }
                    }));
            });
        });

    }

});